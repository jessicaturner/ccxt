<?php include "ccxt.php";
//echo '<pre>'; var_dump (\ccxt\Exchange::$exchanges); echo '</pre>'; 
require_once('php/poloniex.php');

$poloniex = new \ccxt\poloniex ();
$bittrex  = new \ccxt\bittrex  (array ('verbose' => true));
//require_once('php/quoinex.php');
//$quoinex  = new \ccxt\quoinex   ();
$zaif     = new \ccxt\zaif     (array (
    'apiKey' => 'YOUR_PUBLIC_API_KEY',
    'secret' => 'YOUR_SECRET_PRIVATE_KEY',
));
$hitbtc   = new \ccxt\hitbtc   (array (
    'apiKey' => 'YOUR_PUBLIC_API_KEY',
    'secret' => 'YOUR_SECRET_PRIVATE_KEY',
));

$exchange_id = 'binance';
$exchange_class = "\\ccxt\\$exchange_id";
$exchange = new $exchange_class (array (
    'apiKey' => 'YOUR_API_KEY',
    'secret' => 'YOUR_SECRET',
));

$poloniex_markets = $poloniex->load_markets ();

echo '<pre>'; var_dump ($poloniex_markets); echo '</pre><br>'; 
echo '<pre>'; var_dump ($bittrex->load_markets ()); echo '</pre><br>'; 
//var_dump ($quoinex->load_markets ());

echo '<pre>'; var_dump ($poloniex->fetch_order_book ($poloniex->symbols[0])); echo '</pre><br>'; 
echo '<pre>'; var_dump ($bittrex->fetch_trades ('BTC/USD')); echo '</pre><br>'; 
//var_dump ($quoinex->fetch_ticker ('ETH/EUR'));
echo '<pre>'; var_dump ($zaif->fetch_ticker ('BTC/JPY')); echo '</pre><br>'; 

echo '<pre>'; var_dump ($zaif->fetch_balance ()); echo '</pre><br>'; 

// sell 1 BTC/JPY for market price, you pay ¥ and receive ฿ immediately
echo '<pre>'; var_dump ($zaif->id, $zaif->create_market_sell_order ('BTC/JPY', 1)); echo '</pre><br>'; 

// buy BTC/JPY, you receive ฿1 for ¥285000 when the order closes
echo '<pre>'; var_dump ($zaif->id, $zaif->create_limit_buy_order ('BTC/JPY', 1, 285000)); echo '</pre><br>'; 

// set a custom user-defined id to your order
$hitbtc->create_order ('BTC/USD', 'limit', 'buy', 1, 3000, array ('clientOrderId' => '123'));
?>